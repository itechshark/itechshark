About:

iTechshark is not your average repair store. We are your local Apple specialist. Our services include iPhone, iPad, & Mac Sales, Repair, and service. 100% free diagnostic and repair times much quicker than the Apple store. iTechshark sells certified pre-owned Apple devices with a warranty.

Address: 8450 Eager Road, Brentwood, MO 63144

Phone: 314-884-3232
